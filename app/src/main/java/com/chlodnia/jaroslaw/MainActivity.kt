package com.chlodnia.jaroslaw

import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.ImageView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import com.chlodnia.jaroslaw.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var mainUserImageView: ImageView
    private var imagesIdsForImageView: MutableList<Int> = mutableListOf()
    private var currentIndexOfImage: Int = 0
    val imageFromCamera: Uri = Uri.EMPTY
    val takePicture = registerForActivityResult(ActivityResultContracts.TakePicture()) {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        mainUserImageView = binding.imageView
        restoreDefaultDrawableIds()
    }

    fun onImageCheckboxClicked(view: View) {
        if (view is CheckBox) {
            if (view == binding.checkBoxImageEnabled) {
                mainUserImageView.visibility = if (view.isChecked) View.VISIBLE else View.INVISIBLE
            }
        }
    }

    fun onEditTextClicked(view: View) {
        if (view is EditText) {
            val newValue = view.text.toString().toFloatOrNull()
            if (newValue != null && newValue in -Float.MAX_VALUE..Float.MAX_VALUE) {
                when (view) {
                    binding.editTextNumberScale -> {
                        mainUserImageView.scaleX = newValue
                        mainUserImageView.scaleY = newValue
                    }
                    binding.editTextNumberRotation -> {
                        mainUserImageView.rotation = newValue
                    }
                }
            } else view.setText(R.string.input_error_wrong_number)
        }
    }

    fun onButtonNextOrPreviousClicked(view: View) {
        if (view is Button) {
            var newImageIndex = currentIndexOfImage
            when (view) {
                binding.buttonNext -> {
                    newImageIndex += 1
                }
                binding.buttonPrevious -> {
                    newImageIndex -= 1
                }
            }
            setDrawableInImageViewFromSavedImages(newImageIndex)
        }
    }

    private fun setDrawableInImageViewFromSavedImages(index: Int) {
        if (index in 0 until imagesIdsForImageView.size) currentIndexOfImage = index
        mainUserImageView.setImageResource(imagesIdsForImageView[currentIndexOfImage])
    }

    private fun restoreDefaultDrawableIds() {
        imagesIdsForImageView.clear()
        imagesIdsForImageView = mutableListOf(
            R.drawable.img_jaroslaw_1,
            R.drawable.img_jaroslaw_2,
            R.drawable.img_jaroslaw_3,
            R.drawable.img_jaroslaw_4,
            R.drawable.img_hitler_1
        )
        setDrawableInImageViewFromSavedImages(0)
    }

    fun onButtonCleanClicked(view: View) {
        if (view is Button && view == binding.buttonCleanImageView) mainUserImageView.setImageResource(
            android.R.color.transparent
        )
    }

    fun onCameraButtonClick(view: View) {
        takePicture.launch(imageFromCamera)
    }
}